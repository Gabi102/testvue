import Vue from 'vue'
import App from './App.vue'
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  Vue,
  dsn: "https://dd191fe7ba6846f58083ce064aeb3a8b@o487864.ingest.sentry.io/5547111",
  integrations: [
    new Integrations.BrowserTracing(),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
